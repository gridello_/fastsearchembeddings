"""
Random Reference Objects, or Lipschitz Embedding

"""

import numpy as np


class LipschitzEmbedding:

    """
    Note that this is the Vleugels and Veltkamp[1] version: pivots objects (vantage objects in the paper)
    are chosen randomly among the dataset.
    This version was chosen over the original Lipschitz embedding because of its computational efficiency.

    [1]:
    Efficient image retrieval through vantage objects
    http://www.cs.uu.nl/groups/MG/multimedia/publications/art/pr2002.pdf
    """

    def __init__(self, n_pivots, dist_f):
        """

        :param n_pivots: float between 0 and 1 for frac, or int
        """
        if type(n_pivots) == float:
            assert 0 < n_pivots < 1

        elif type(n_pivots) == int:
            assert n_pivots > 0
        else:
            raise ValueError('n_pivots malformed')
        self.n_pivots = n_pivots
        self.pivots = None
        self.dist_f = dist_f

    def fit(self, data):
        """
        Creates the pivots
        :param data: array
        :return:
        """
        n_pivots = self.n_pivots if type(self.n_pivots) == int else int(len(data) * self.n_pivots)
        self.pivots = np.random.choice(data, n_pivots, replace=False)
        return self

    def transform_item(self, item):
        """
        Return an
        :param item:
        :return:
        """
        out = []
        for p in self.pivots:
            out.append(self.dist_f(item, p))
        return np.array(out)

    def transform(self, data):
        """
        Returns the embedded data: a len(data) x self.n_pivots matrix
        :param data:
        :return:
        """
        out = []
        for item in data:
            out.append(self.transform_item(item))
        return np.array(out)

    def fit_transform(self, data):
        self.fit(data)
        return self.transform(data)

    def distortion(self, data, emb_f):
        """
        Computationally heavy!
        Distortion is a measure of the quality of the embedding.
        Distortion measures how much larger or smaller the distances in the embedding space are than the
        corresponding distances in the original space

        :param emb_f: distance function in the embedded space. Should probably be euclidean
        :param data: data on which to measure distortion. Measuring distortion itself is O(N^2), so beware.
        :return:
        """

        inv_c1 = c2 = 1
        for a in data:
            for b in data:
                d = self.dist_f(a, b)
                emb_a = self.transform_item(a)
                emb_b = self.transform_item(b)
                f_d = emb_f(emb_a, emb_b)

                if f_d > d:
                    tmp_c2 = f_d / d
                    if tmp_c2 > c2:
                        c2 = tmp_c2

                elif f_d < d:
                    tmp_inv_c1 = f_d / d
                    if tmp_inv_c1 < inv_c1:
                        inv_c1 = tmp_inv_c1

        return 1/inv_c1, c2

    def stress(self, data, emb_f):
        """
        Computationally heavy!
        Stress measures the overall deviation in the distances (i.e., the extent to which they differ).
        We here measure it in terms of variance.

        See http://www.cs.umd.edu/~hjs/pubs/hjaltasonpami03.pdf for more info
        :param emb_f: distance function in the embedded space. Should probably be euclidean
        :param data:
        :return:
        """
        sum_numerator = sum_denominator = 0
        for a in data:
            for b in data:
                d = self.dist_f(a, b)
                emb_a = self.transform_item(a)
                emb_b = self.transform_item(b)
                f_d = emb_f(emb_a, emb_b)

                sum_numerator += np.square(f_d - d)
                sum_denominator += np.square(d)
        return sum_numerator / sum_denominator
