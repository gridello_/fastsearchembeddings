"""
FastMap
In development

FastMap is an embedding method not dissimilar to dimensionality reduction methods.
The objective is to embed the original space into an Euclidean one. FastMap does so by projecting objects onto
"lines". Each line is constructed by choosing two pivot-objects of the original space. The pivots are chosen
to maximize variance on the line they create.

For more information, [1] provides an excellent overview of Fastmap.

[1]: http://www.cs.umd.edu/~hjs/pubs/hjaltasonpami03.pdf
"""

import numpy as np
from scipy.spatial.distance import euclidean
from copy import deepcopy


class FastMap:
    """
    In development.
    Pivots are chosen as in the paper.
    Lots of methods to add.

    Objects in the original space become (object, [embedded features vector]). This tuple is referred to as
    an item in the following code, and the vector of features is f_object.

    """

    def __init__(self, n_projections, dist_f):
        """

        :param n_projections: float between 0 and 1 for frac, or int
        """
        if type(n_projections) == float:
            assert 0 < n_projections < 1

        elif type(n_projections) == int:
            assert n_projections > 0
        else:
            raise ValueError('n_projections malformed')
        self.n_projections = n_projections
        self.pivots = None
        self.dist_f = dist_f

    def pair_distance(self, item_a, item_b):
        """
        Internal item to item distance
        Returns self.dist(a,b) if f_data is empty.
        Returns the distance as defined in fastmap otherwise.
        :param item_a:
        :param item_b:
        :return:
        """
        a, f_a = item_a
        b, f_b = item_b
        d_ab = self.dist_f(a, b)

        if not f_a:  # first round, f_data not populated yet.
            return d_ab
        de_a_b = euclidean(f_a, f_b)

        d_squared = d_ab ** 2 - de_a_b ** 2
        if d_squared >= 0:
            return np.sqrt(d_squared)
        else:
            return -np.sqrt(-d_squared)  # See [1] for more information

    def _distance_symmetric(self, items):
        """
        Distance function for pairwise elements of a set on itself.
        Since the matrix returned is symmetric, we only have to do calculations for the upper triangle.
        :param items: list of items (transformed space)
        :return: matrix of pairwise distances
        """
        n = len(items)
        out = np.zeros((n, n))
        for i in range(n):
            for j in range(i+1, n):
                out[i][j] = out[j][i] = self.pair_distance(items[i], items[j])
        return out

    def distance(self, items_a, items_b=None):
        """
        If items b is not given, same as distance(items_a, items_a)
        :param items_a: list of items (transformed space)
        :param items_b: list of items (transformed space)
        :return: matrix of pairwise distances
        """
        if not items_b:
            return self._distance_symmetric(items_a)

        n_a = len(items_a)
        n_b = len(items_b)
        out = np.zeros((n_a, n_b))

        for i in range(n_a):
            for j in range(n_b):
                out[i][j] = self.pair_distance(items_a[i], items_b[j])
        return out

    def farthest(self, item, data):
        """
        Runs all distances from an item to the dataset and return the farthest point and the max distance
        :param item:
        :param data:
        :return:
        """
        max_d = 0
        farthest = None
        for object2 in data:
            d = self.pair_distance(item, object2)
            if d > max_d:
                max_d = d
                farthest = object2
        return farthest, max_d

    def projection_dist(self, item, pivot_couple):
        """
        As defined in fastmap. Basically the distance to a projection onto a line formed by the pivots.
        :param item:
        :param pivot_couple: (object, object)
        :return:

        """
        p1, p2 = pivot_couple
        d_o_p1 = self.pair_distance(item, p1)
        d_o_p2 = self.pair_distance(item, p2)
        d_p1_p2 = self.pair_distance(p1, p2)

        return (d_o_p1 ** 2 + d_p1_p2 ** 2 - d_o_p2 ** 2) / (2 * d_p1_p2)

    def find_pivot(self, f_data, n_trials=5, len_f_data=None):
        """
        Tries to find the 2 farthest points, whose line will be defined as the next pivot.
        To do so:
            - pick a point at random, call it a
            - find the farthest point from a, call it r
            - find the farthest point from r, call it s
            - do the above a handful of times to get an approximation of the best pivot.

        :param f_data:
        :param n_trials:
        :param len_f_data: =len(f_data), provided for computation reduction
        :return:
        """
        if not len_f_data:
            len_f_data = len(f_data)

        max_rs_dist = 0
        r = s = None
        for _ in range(n_trials):
            a = f_data[np.random.randint(0, len_f_data)]
            tmp_r, _ = self.farthest(a, f_data)
            tmp_s, rs_dist = self.farthest(tmp_r, f_data)
            if rs_dist > max_rs_dist:
                max_rs_dist = rs_dist
                r = tmp_r
                s = tmp_s
        return r, s

    def fit(self, data):
        """
        Updates the pivots and transforms the given data
        :param data:
        :return:
        """
        n = len(data)
        self.pivots = []
        f_data = [(x, []) for x in data]
        for round_number in range(self.n_projections):
            self.pivots.append(deepcopy(self.find_pivot(f_data)))
            to_append = []
            for o in f_data:
                x_o = self.projection_dist(o, self.pivots[round_number])
                to_append.append(x_o)
            for i in range(n):
                f_data[i][1].append(to_append[i])
        return f_data

    def transform(self, data):
        """
        Transforms the data with the instance's pivots
        :param data: object or [object], where object can be any object in the original space
        :return:
        """
        if not type(data) in [list, np.ndarray]:
            data = [data]

        n = len(data)
        f_data = [(x, []) for x in data]
        for round_number in range(self.n_projections):
            to_append = []
            for o in f_data:
                x_o = self.projection_dist(o, self.pivots[round_number])
                to_append.append(x_o)
            for i in range(n):
                f_data[i][1].append(to_append[i])
        return f_data

    def fit_transform(self, data):
        return self.fit(data)

    def distortion(self, data):
        """
        Computationally heavy!
        Distortion is a measure of the quality of the embedding.
        Distortion measures how much larger or smaller the distances in the embedding space are than the
        corresponding distances in the original space

        :param data: data (original space) on which to measure distortion. Measuring distortion is O(N^2)
        :return:
        """

        inv_c1 = c2 = 1
        for a in data:
            for b in data:
                d = self.dist_f(a, b)
                f_a = self.transform(a)
                f_b = self.transform(b)
                f_d = self.distance(f_a, f_b)

                if f_d > d:
                    tmp_c2 = f_d / d
                    if tmp_c2 > c2:
                        c2 = tmp_c2

                elif f_d < d:
                    tmp_inv_c1 = f_d / d
                    if tmp_inv_c1 < inv_c1:
                        inv_c1 = tmp_inv_c1

        return 1/inv_c1, c2

    def stress(self, data):
        """
        Computationally heavy!
        Stress measures the overall deviation in the distances (i.e., the extent to which they differ).
        We here measure it in terms of variance.

        See [1] for more info
        :param data: data (original space) on which to measure stress. Measuring stress is O(N^2)
        :return:
        """
        sum_numerator = sum_denominator = 0
        for a in data:
            for b in data:
                d = self.dist_f(a, b)
                f_a = self.transform(a)
                f_b = self.transform(b)
                f_d = self.distance(f_a, f_b)

                sum_numerator += np.square(f_d - d)
                sum_denominator += np.square(d)
        return sum_numerator / sum_denominator
