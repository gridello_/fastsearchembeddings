import numpy as np
from sklearn.metrics.pairwise import cosine_similarity


def arg_top_n(vec, kind, top_n=5):
    """
    Return the arg of the top_n max (resp. min) in a vector of similarity (resp. distance)

    :param vec: array to be worked on 
    :param top_n:h  ow many indices to retrieve
    :param kind: {'similarity', 'distance'} 
    :return:
    """
    assert kind in ['similarity', 'distance']

    try:
        if kind == 'similarity':
            idx = np.argpartition(vec, kth=-top_n)[0][-top_n:] 
        elif kind == 'distance':
            idx = np.argpartition(vec, kth=top_n)[0][:top_n]

    except ValueError:
        idx = np.arange(len(vec))

    return idx


def cosine_similarity_closest(query, searchlist, top_n=5):
    """
    TODO rn handles only vector query (i.e. a single item)
    Returns

    :param query: array, features of the query
    :param searchlist: matrix, features of candidate neighbors
    :param top_n: hw many neighbors we look for
    :return: {idx: score} for the top_n closest items
    """

    cs = cosine_similarity(query, searchlist)
    idx = arg_top_n(cs, kind='similarity', top_n=top_n)
    scores = cs[0][idx]
    return {idx[i]: scores[i] for i in range(len(idx))}