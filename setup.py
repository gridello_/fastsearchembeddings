from setuptools import setup, find_packages


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='FastSearchEmbeddings',
    version='0.1',
    packages=find_packages(),
    description='Some embeddings',
    long_description=readme(),
    zip_safe=False, install_requires=['numpy']

)
